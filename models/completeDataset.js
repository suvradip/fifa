module.exports = (sequelize, DataTypes) => sequelize.define(
   'completedataset',
   {
      rowId: {
         type: DataTypes.INTEGER(11),
         allowNull: false,
         primaryKey: true,
      },
      Name: {
         type: DataTypes.STRING(17),
      },
      Age: {
         type: DataTypes.INTEGER(11),
      },
      Photo: {
         type: DataTypes.STRING(47),
      },
      Nationality: {
         type: DataTypes.STRING(9),
      },
      Flag: {
         type: DataTypes.STRING(35),
      },
      Overall: {
         type: DataTypes.INTEGER(11),
      },
      Potential: {
         type: DataTypes.INTEGER(11),
      },
      Club: {
         type: DataTypes.STRING(19),
      },
      Club_Logo: {
         type: DataTypes.STRING(42),
      },
      Value: {
         type: DataTypes.STRING(6),
      },
      Wage: {
         type: DataTypes.STRING(5),
      },
      Special: {
         type: DataTypes.INTEGER(11),
      },
      Acceleration: {
         type: DataTypes.INTEGER(11),
      },
      Aggression: {
         type: DataTypes.INTEGER(11),
      },
      Agility: {
         type: DataTypes.INTEGER(11),
      },
      Balance: {
         type: DataTypes.INTEGER(11),
      },
      Ball_control: {
         type: DataTypes.INTEGER(11),
      },
      Composure: {
         type: DataTypes.INTEGER(11),
      },
      Crossing: {
         type: DataTypes.INTEGER(11),
      },
      Curve: {
         type: DataTypes.INTEGER(11),
      },
      Dribbling: {
         type: DataTypes.INTEGER(11),
      },
      Finishing: {
         type: DataTypes.INTEGER(11),
      },
      Free_kick_accuracy: {
         type: DataTypes.INTEGER(11),
      },
      GK_diving: {
         type: DataTypes.INTEGER(11),
      },
      GK_handling: {
         type: DataTypes.INTEGER(11),
      },
      GK_kicking: {
         type: DataTypes.INTEGER(11),
      },
      GK_positioning: {
         type: DataTypes.INTEGER(11),
      },
      GK_reflexes: {
         type: DataTypes.INTEGER(11),
      },
      Heading_accuracy: {
         type: DataTypes.INTEGER(11),
      },
      Interceptions: {
         type: DataTypes.INTEGER(11),
      },
      Jumping: {
         type: DataTypes.INTEGER(11),
      },
      Long_passing: {
         type: DataTypes.INTEGER(11),
      },
      Long_shots: {
         type: DataTypes.INTEGER(11),
      },
      Marking: {
         type: DataTypes.INTEGER(11),
      },
      Penalties: {
         type: DataTypes.INTEGER(11),
      },
      Positioning: {
         type: DataTypes.INTEGER(11),
      },
      Reactions: {
         type: DataTypes.INTEGER(11),
      },
      Short_passing: {
         type: DataTypes.INTEGER(11),
      },
      Shot_power: {
         type: DataTypes.INTEGER(11),
      },
      Sliding_tackle: {
         type: DataTypes.INTEGER(11),
      },
      Sprint_speed: {
         type: DataTypes.INTEGER(11),
      },
      Stamina: {
         type: DataTypes.INTEGER(11),
      },
      Standing_tackle: {
         type: DataTypes.INTEGER(11),
      },
      Strength: {
         type: DataTypes.INTEGER(11),
      },
      Vision: {
         type: DataTypes.INTEGER(11),
      },
      Volleys: {
         type: DataTypes.INTEGER(11),
      },
      CAM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      CB: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      CDM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      CF: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      CM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      ID: {
         type: DataTypes.INTEGER(11),
      },
      LAM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      LB: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      LCB: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      LCM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      LDM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      LF: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      LM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      LS: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      LW: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      LWB: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      Preferred_Positions: {
         type: DataTypes.STRING(6),
      },
      RAM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      RB: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      RCB: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      RCM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      RDM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      RF: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      RM: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      RS: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      RW: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      RWB: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
      ST: {
         type: DataTypes.DECIMAL,
         allowNull: true,
      },
   },
   {
      tableName: 'completedataset',
   }
);
