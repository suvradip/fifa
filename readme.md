# Fifa backend api(s)

### Deployment instructions,

npm i
npm run importData
npm start

## Project setup

It is a node js based project so you need to do a `npm i` to install project dependencies. You need to copy .env.example file and create a `.env` file in project root and define those variables.

`npm run importData` - to migrate database
`npm start`
OR
`npm run dev` - for developing mode

### According to tasks followinng things are present

    - [*] Dump the FIFA data set that is provided into your MySQL database and create the required schema.
    - [*] Authentication of the incoming request.
    - [*] Validate the API key and JSON schema

### Api endpoints

- get_player_info (/api/get_player_info)

```bash
	curl -X GET \
	http://127.0.0.1:3000/api/get_player_info \
	-H 'Authorization: Api-Key xyz-abc' \
	-H 'Content-Type: application/json' \
	-d '{ "name": "Cristiano Ronaldo" }'
```

- get_club_player_list (/api/get_club_player_list)

```bash
	curl -X GET \
	http://localhost:3000/api/get_club_player_list/ \
	-H 'Authorization: Api-Key  xyz-abc' \
	-H 'Content-Type: application/json' \
	-d '{ "club": "Real Madrid CF" }'
```

> There are two KEYS `xyz-abc` & `abc-xyz`, you can use any of them.

you need to pass a api key to retrive results. You need to set api key as `Authorization` header.
Example -

```bash
curl -v --header "Authorization: Api-Key abc-xyz" http://127.0.0.1:3000/api/get_player_info
```
