const express = require('express');
const { validationResult } = require('express-validator/check');
const db = require('../config/sequelize');
const validator = require('./validation');

const router = express.Router();


router.get('/get_player_info', validator.playerInfo(), async (req, res) => {
   const errors = validationResult(req);
   if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
   }

   const { name } = req.body;
   const response = await db.completeDataset.findAll({
      where: { Name: name },
   });
   res.json(response);
});


router.get('/get_club_player_list', validator.clubInfo(), async (req, res) => {
   const errors = validationResult(req);
   if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
   }

   const { club } = req.body;
   const response = await db.completeDataset.findAll({
      where: { Club: club },
   });
   return res.json(response);
});


module.exports = router;
