const express = require('express');
const passport = require('passport');
const { HeaderAPIKeyStrategy } = require('passport-headerapikey');
const apiRoutes = require('./api');

const router = express.Router();

/**
 * for now defining two static keys
 */
const KEYS = [
   'xyz-abc',
   'abc-xyz',
];

/**
 * Using passportjs and it's helper methods to check api key
 */
passport.use(new HeaderAPIKeyStrategy(
   { header: 'Authorization', prefix: 'Api-Key ' },
   false,
   ((apikey, done) => {
      if (apikey && KEYS.indexOf(apikey.trim()) > -1) return done(null, true);
      return done(null, false);
   })
));


/**
 * A general route
 */
router.get('/', (req, res, next) => {
   res
      .json({
         msg: 'Working',
      })
      .sendStatus(200);
});

/**
 * Api routes
 */
router.use('/api', passport.authenticate('headerapikey', { session: false }), apiRoutes);

module.exports = router;
