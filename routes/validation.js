/**
 * user data validations before performing any operaions
 */
const { body } = require('express-validator/check');

module.exports = {
   playerInfo: () => [
      body('name', "name doesn't exists").exists(),
   ],

   clubInfo: () => [
      body('club', "club name doesn't exists").exists(),
   ],
};
