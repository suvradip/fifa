require('dotenv').config();
const csv = require('csv-parser');
const fs = require('fs');
const path = require('path');
const db = require('../config/sequelize');

const completeDatasetModel = db.completeDataset;


async function inserData(data, model) {
   try {
      await model.create(data);
   } catch (error) {
      console.log('data insertion failed', error);
      process.exit();
   }
}

fs.createReadStream(path.resolve('data/csvs/CompleteDataset.csv'))
   .pipe(csv())
   .on('data', async (obj) => {
      await inserData(obj, completeDatasetModel);
   })
   .on('end', () => {
      console.log('completeDatasetModel import completed.');
      process.exit();
   });
